#!/bin/sh

K8S_EKS_NAME="${K8S_EKS_NAME:-stackgres-e2e}"
K8S_VERSION="${K8S_VERSION:-1.15.10}"
if [ "$K8S_VERSION" = "$DEFAULT_K8S_VERSION" ]
then
  >&2 echo "Warning: using kubernetes version 1.13.11 since e2e default $DEFAULT_K8S_VERSION is not available for eks"
  K8S_VERSION=1.13.11
fi
K8S_EKS_REGION="${K8S_EKS_REGION:-us-west-2}"
K8S_EKS_NODE_TYPE="${K8S_EKS_NODE_TYPE:-m5.large}"
K8S_EKS_DISK_SIZE="${K8S_EKS_DISK_SIZE:-20}"
K8S_EKS_OPTS="$K8S_EKS_OPTS"

export E2E_USE_INTERMIDIATE_PRIVATE_REPO="${E2E_USE_INTERMIDIATE_PRIVATE_REPO:-true}"
export E2E_OPERATOR_PULL_POLICY=Always

export K8S_EKS_NAME K8S_VERSION K8S_EKS_REGION K8S_EKS_NODE_LOCATIONS K8S_EKS_NODE_TYPE K8S_EKS_OPTS

get_k8s_env_version() {
  echo "eksctl version $(eksctl version)"
  echo
}

reuse_k8s() {
  if ! eksctl get cluster --name "$K8S_EKS_NAME" --region "$K8S_EKS_REGION" 2>&1 \
    | grep "^$K8S_EKS_NAME" | grep -q "ACTIVE"
  then
    echo "Can not reuse eks environment $K8S_EKS_NAME"
    exit 1
  fi

  echo "Reusing eks environment $K8S_EKS_NAME"

  aws eks update-kubeconfig --name "$K8S_EKS_NAME" --region "$K8S_EKS_REGION"
}

reset_k8s() {
  echo "Setting up eks environment $K8S_EKS_NAME..."

  delete_k8s
  eksctl create cluster --name "$K8S_EKS_NAME" \
    --region "$K8S_EKS_REGION" \
    --spot \
    --node-type "$K8S_EKS_NODE_TYPE" \
    --node-volume-size "$K8S_EKS_DISK_SIZE" \
    --nodes 3 \
    --version "$(echo "$K8S_VERSION" | cut -d . -f 1-2)" \
    $K8S_EKS_OPTS

  echo "...done"
}

delete_k8s() {
  echo "Deleting eks environment $K8S_EKS_NAME..."

  if eksctl get cluster --name "$K8S_EKS_NAME" --region "$K8S_EKS_REGION" 2>&1 \
    | grep "^$K8S_EKS_NAME" | grep -q "ACTIVE"
  then
    eksctl delete cluster --wait --name "$K8S_EKS_NAME" --region "$K8S_EKS_REGION" || true
  fi
  
  aws ec2 describe-volumes --region "$K8S_EKS_REGION" --filters "Name=tag-key,Values=kubernetes.io/cluster/$K8S_EKS_NAME" \
    | jq -r '.Volumes[].VolumeId' | xargs -r -n 1 -I % sh -c "aws ec2 detach-volume --force --region $K8S_EKS_REGION --volume-id % || true"
  
  aws ec2 describe-volumes --region "$K8S_EKS_REGION" --filters "Name=tag-key,Values=kubernetes.io/cluster/$K8S_EKS_NAME" \
    | jq -r '.Volumes[].VolumeId' | xargs -r -n 1 -I % sh -c "aws ec2 delete-volume --region $K8S_EKS_REGION --volume-id % || true" 

  echo "...done"
}

load_image_k8s() {
  echo "Cannot load images directly to k8s in a eks environment"
  exit 1
}

get_k8s_versions() {
  cat << EOF
1.16.8
1.15.11
1.14.9
1.13.12
EOF
}

excluded_validatingwebhookconfigurations() {
  echo "vpc-resource-validating-webhook"
}

excluded_mutatingwebhookconfigurations() {
  echo "pod-identity-webhook"
  echo "vpc-resource-mutating-webhook"
}

excluded_customresourcedefinitions() {
  echo ".*\.amazonaws\.com"
  echo ".*\.k8s\.aws"
}

excluded_podsecuritypolicies() {
  echo "eks\.privileged"
}

excluded_clusterroles() {
  echo "aws-node"
  echo "eks:.*"
  echo "vpc-resource-controller-role"
}

excluded_clusterrolebindings() {
  echo "aws-node"
  echo "eks:.*"
  echo "vpc-resource-controller-rolebinding"
}
